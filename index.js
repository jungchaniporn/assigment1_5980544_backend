const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const Contacts = require('./src/Router/router');
const logger = require('./config/logger');

dotenv.config();
mongoose.Promise = global.Promise;
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/contacts', Contacts);

mongoose.connect('mongodb+srv://admin:admin@cluster0-sfjoi.gcp.mongodb.net/Assignment?retryWrites=true&w=majority', { useNewUrlParser: true }, (error) => {
  if (error) throw error;
  logger.info('Successfully connected');
  // console.log('Successfully connected');
});
app.listen(process.env.port);


logger.info('Name'.includes('am'));

logger.info(`listening to port : ${process.env.port}`);
